class SendActivity
  include SuckerPunch::Job

  def perform(target, action, ids)
    ActivityIntegration.activity_id = ids[:activity_id]
    ActivityIntegration.pm_activity_id = ids[:pm_activity_id]
    ActivityIntegration.activity_action = action
    ActivityIntegration.send_activity(target)
  end
end
