class ActivityIntegrationController < ActionController::Base

  def update_time_entry
    (TimeEntry.find(params[:activity_id]).update_column(:pm_activity_id, params[:id]) rescue false) ? (head :ok) : (head :forbidden)
  end

  def update_notify_accept
    project = Project.find_by_identifier(params[:name])
    project&.update(send_notify: params[:val]) ? (head :ok) : (head :forbidden)
  end
end
