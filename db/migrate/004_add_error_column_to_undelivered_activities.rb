class AddErrorColumnToUndeliveredActivities < ActiveRecord::Migration
  def change
    add_column :undelivered_activities, :error, :string
  end
end
