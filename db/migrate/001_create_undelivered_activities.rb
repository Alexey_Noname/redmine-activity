class CreateUndeliveredActivities < ActiveRecord::Migration
  def change
    create_table :undelivered_activities do |t|
      t.string  :action
      t.string  :task
      t.string  :project
      t.string  :user
      t.string  :comment
      t.date    :date
      t.float   :hours
      t.integer :activity_id
      t.integer :pm_activity_id
    end
  end
end
