class AddPmIdColumnToTimeEntry < ActiveRecord::Migration
  def change
    add_column :time_entries, :pm_activity_id, :integer
  end
end
