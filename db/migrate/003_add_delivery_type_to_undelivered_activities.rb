class AddDeliveryTypeToUndeliveredActivities < ActiveRecord::Migration
  def change
    add_column :undelivered_activities, :delivery_type, :string
  end
end
