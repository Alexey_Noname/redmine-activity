class AddSendNotifyColumnToProjects < ActiveRecord::Migration
  def change
    add_column :projects, :send_notify, :boolean, default: false
  end
end
