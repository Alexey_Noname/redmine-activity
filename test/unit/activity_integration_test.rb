require_relative '../test_helper'

class ActivityIntegrationTest < ActiveSupport::TestCase
  include ActivityIntegrationHelpers

  fixtures :projects, :users, :email_addresses, :user_preferences, :members, :member_roles, :roles,
           :groups_users,
           :trackers, :projects_trackers,
           :enabled_modules,
           :versions,
           :issue_statuses, :issue_categories, :issue_relations, :workflows,
           :enumerations,
           :issues, :journals, :journal_details,
           :custom_fields, :custom_fields_projects, :custom_fields_trackers, :custom_values,
           :time_entries

  test 'project send_notify column exists' do
    assert Project.column_names.include?('send_notify'), 'Project dont have required column'
  end

  test 'TimeEntry defined' do
    assert((Object.const_get(:TimeEntry).is_a?(Class) rescue false), 'does not work without plugin spent time')
  end

  test 'methods defined' do
    assert TimeEntry.instance_methods.include?(:send_activity), 'send_activity method not defined'
    assert TimeEntry.instance_methods.include?(:check_state), 'check_state method not defined'
    assert TimeEntry.instance_methods.include?(:define_ids), 'define_ids method not defined'
  end

  test 'callbacks defined' do
    assert callback_defined?(:_save_callbacks, :before, :check_state), 'before_save callback not defined'
    assert callback_defined?(:_destroy_callbacks, :after, :check_state), 'after_destroy callback not defined'
    assert callback_defined?(:_commit_callbacks, :after, :send_activity), 'after_commit callback not defined'
  end

  test 'check api variables' do
    assert_equal 'http://pm.nasctech.com/api/v1/custom_objects/redmine_api/activity',
    ActivityIntegration.instance_variable_get('@pm_api_url'), 'variable @pm_api_url dont equal'
    assert_equal 'http://padrino.whaleberry.com/activity/',
    ActivityIntegration.instance_variable_get('@notify_api_url'), 'variable @notify_api_url dont equal'
    assert_equal 'http://padrino.whaleberry.com/activity/undelivered',
    ActivityIntegration.instance_variable_get('@undelivered_api_url'), 'variable @undelivered_api_url dont equal'
  end

  test 'serialize method works current' do
    time_entry = TimeEntry.last
    serialized = ActivityIntegration.serialize(time_entry)
    assert_equal time_entry.project.name, serialized[:project], 'project not match'
    assert_equal time_entry.spent_on, serialized[:date], 'date not match'
    assert_equal time_entry.user.login, serialized[:user], 'user not match'
    assert_equal time_entry.comments, serialized[:comment], 'comment not match'
    assert_equal time_entry.hours, serialized[:hours], 'hours not match'
  end

  test 'send_activity dont work without project accept' do
    pm_id = rand(999..9999)
    FakeWeb.register_uri(:post, "http://pm.nasctech.com/api/v1/custom_objects/redmine_api/activity",
    body: {status: 'ok', mess: pm_id}.to_json, status: ["200", "OK"])
    FakeWeb.register_uri(:post, "http://padrino.whaleberry.com/activity/create", body: {status: 'ok'}.to_json, status: ["200", "OK"])
    time_entry = Issue.first.time_entries.create(user: User.second, hours: 3, comments: 'test', spent_on: Time.now)
    assert time_entry.reload.pm_activity_id.blank?, 'pm_activity_id present'
  end

  ['create', 'update', 'destroy'].each{|action|
    test "on #{action} with valid pm response" do
      pm_id = rand(999..9999)
      FakeWeb.register_uri(:post, "http://pm.nasctech.com/api/v1/custom_objects/redmine_api/activity",
      body: {status: 'ok', mess: pm_id}.to_json, status: ["200", "OK"])
      action_helper(action, pm_id)
      assert_equal 0, UndeliveredActivity.count
    end

    test "on #{action} with invalid pm response" do
      pm_id = rand(999..9999)
      FakeWeb.register_uri(:post, "http://pm.nasctech.com/api/v1/custom_objects/redmine_api/activity", body: 'some text', status: ["200", "OK"])
      FakeWeb.register_uri(:post, "http://padrino.whaleberry.com/activity/undelivered", body: {status: 'ok'}.to_json, status: ["200", "OK"])
      Issue.first.project.update(send_notify: true)
      time_entry = Issue.first.time_entries.create(user: User.second, hours: 3, comments: 'test', spent_on: Time.now)
      action_helper(action)
      assert_equal 0, UndeliveredActivity.count
    end

    test "on #{action} with invalid pm and padrino response" do
      pm_id = rand(999..9999)
      FakeWeb.register_uri(:post, "http://pm.nasctech.com/api/v1/custom_objects/redmine_api/activity", body: 'some text', status: ["200", "OK"])
      FakeWeb.register_uri(:post, "http://padrino.whaleberry.com/activity/undelivered", status: ["503", "Service Unavailable"])
      Issue.first.project.update(send_notify: true)
      action_helper(action)
      assert_equal 1, UndeliveredActivity.count
    end
  }
end
